 
================
Troubleshooting
================

.. contents:: Table of Contents
    :local:
    
Q&A
------
* second robot instance does not stream cameras.
| - make sure all cameras ports are uniqe across all robots in a simulation.
| - also make sure those ports are not taken by another running software.
* can't join server in multyplayer mode.
| - try disabling linux firewall by running `sudo ufw disable`
* the simulation crashes when running complex level
| - try lowering the quality on the scalability widget
| - try freeing RAM by closing other running apps

Known issues
--------------
* Hot keys may not work properly if the keyboard language is not English when the Sphera software is loaded.
* Disconnections and interruptions may cause disconnection from the host level in the Sphera software.
* When running bigger levels, a simulation crash might occur on computers with insufficient RAM

Contact Us
-----------

.. list-table::
   :widths: 1 1 1
   :header-rows: 1
        
   * - Name
     - Role
     - Email
   * - Yam Geva
     - maintainer
     - yam@robotican.net
   * - Omri Eitan
     - maintainer
     - omri.eitan@robotican.net