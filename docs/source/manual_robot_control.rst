 
====================
Manual Robot Control
====================

.. contents:: Table of Contents
    :local:

Modes change
-------------

After spawning and posesing a robot, you may use the following to change between the 3 basic modes.

.. list-table:: Hot Keys
   :widths: 25 100
   :header-rows: 1

   * - Key
     - Function
   * - numpad 1
     - Sit mode
   * - numpad 2
     - Stand mode
   * - numpad 3
     - Walk mode

.. raw:: html

    <div style="position: relative; height: 0; overflow: hidden; max-width: 100%; height: auto; margin-bottom: 3em;">
    <video width="100%" height="auto" controls>
    <source src="_static/videos/change_modes.webm" type="video/webm">
    Your browser does not support the video tag.
    </video>    
    </div>

Pose change in stand mode
--------------------------

While in stand mode, the robot's posture can be manipulated.

.. list-table:: Hot Keys
   :widths: 25 100
   :header-rows: 1

   * - Key
     - Function
   * - Left Alt + click&drag
     - change roll and pitch
   * - Left Shift + click&drag
     - change yaw and stance height

.. raw:: html

    <div style="position: relative; height: 0; overflow: hidden; max-width: 100%; height: auto; margin-bottom: 3em;">
    <video width="100%" height="auto" controls>
    <source src="_static/videos/manual_pose_command.webm" type="video/webm">
    Your browser does not support the video tag.
    </video>    
    </div>

Walk
--------

While in walk mode, the robot can walk around 

.. list-table:: Hot Keys
   :widths: 25 100
   :header-rows: 1

   * - Key
     - Function
   * - w
     - move forwards
   * - s
     - move backwards
   * - a
     - move sideways to the left
   * - d
     - move sideways to the right
   * - click&drag
     - turn
   * - hold space
     - move faster


.. raw:: html

    <div style="position: relative; height: 0; overflow: hidden; max-width: 100%; height: auto; margin-bottom: 3em;">
    <video width="100%" height="auto" controls>
    <source src="_static/videos/manual_walk.webm" type="video/webm">
    Your browser does not support the video tag.
    </video>    
    </div>

flashlights
--------------

toggling flashlights

.. list-table:: Hot Keys
   :widths: 25 100
   :header-rows: 1

   * - Key
     - Function
   * - numpad +
     - toggle front lights on/brighter/off
   * - numpad -
     - toggle back lights on/brighter/off

.. raw:: html

    <div style="position: relative; height: 0; overflow: hidden; max-width: 100%; height: auto; margin-bottom: 3em;">
    <video width="100%" height="auto" controls>
    <source src="_static/videos/flashlights_manual.webm" type="video/webm">
    Your browser does not support the video tag.
    </video>    
    </div>