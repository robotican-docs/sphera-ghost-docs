 
================
Change Log
================

.. contents:: Table of Contents
    :local:
    
Version 1.2.3
---------------
* fixed IMU acceleration.
* added full attitude RPY in ``/<robot_name>/sphera/state``
* added relative pose state for Vision60 - ``/<robot_name>/mcu/state/rel_pose``
* added velocity state for Vision60 - ``/<robot_name>/mcu/state/vel``
* changed pose commands to normalized euler deg, to match the real robot commands.
* **LiDAR updates**: (full doc at 'spawning robots' page, under '3D (Multi Layer) LIDAR Spawn Options')

  * added option to add 'time' & 'ring' fields to LiDAR pointcloud data - ``add_ring_and_time``.
  * added option to set the alignment of the first point index - ``yaw_alignment``.
  * the highest angle towards the sky has a ring index of 0.

* updated sphera_common_interfaces to v1.0.2 (added environment field to ``sphera_common_interfaces.msg.SpheraStatus``)
* improved weather conditions control & realism - see change in the ``SpheraSet`` service, in 'ROS2 Interfaces' page.
* added new map - ``MilitaryAirport``.
* The environment variable ``SPHERA_LICENSE_FILE`` would override license file absolute path if set. 


Version 1.0.14
---------------
* added movement parameters to the Vision60 spawn options
  
  * ``max_walk_linear_speed`` - maximum linear speed of the robot (m/s).
  * ``max_walk_angular_speed`` - maximum angular speed of the robot (deg/s).
  * ``max_acceleration`` - walking acceleration of the robot (m/s^2).
  * ``max_step_height`` - maximum height gap the robot would be able to walk through (meters).
  * ``max_walkable_angle`` - maximum slope angle the robot would be able to walk through (deg).
   
* Added ``/<robot_name>/sphera/set_state`` topic interface to Vision60. used for location resets.
  
  * example usage: ``ros2 topic pub /<robot_name>/sphera/set_state sphera_common_interfaces/msg/SpheraPawnState "{location: {x: 1.0, y: 2.0, z: 0.6}, rotation: {yaw: 90.0}}" --once``
  * can also be useful for rapid data collection.

Version 1.0.13
---------------
* Added new level - **Forest**
* Added collision detection
* Added falling detection
* Added AI walk for a person spawn
* Fixed multiplayer ROS2 interfaces bug
  
Version 1.0.12
---------------
* First release