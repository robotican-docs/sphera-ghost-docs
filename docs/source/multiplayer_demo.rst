 
================================
Multiplayer Demo (Experimental)
================================

.. contents:: Table of Contents
    :local:


Setup
-------

#. have 2 or more instances of Sphera opened in main menu.
#. all instances need to run on the same LAN.
#. same computer is also fine - depending on your computation capabilities.

How to run multiplayer
-----------------------

#. make sure each instance has different **user** set in the main menu
#. start a level with one instance
#. with each of the other instances got to join, wait for refresh, and select the server that was opened.
#. now all instances should be connected to the same simulation.
#. (optional) spawn robots, spawn objects and see them in every sphera instance.

* if you can't find the server in the *join menu*, try disabling linux firewall by running sudo ufw disable.


  .. note::

    Possessing a robot by one player would 'steal' it, if it was currently possessed by another player.

  .. note::

    In order to view a camera's video stream from another computer, make sure to change the *host=<spawner_ip>* in the GStreamer pipeline.

  .. warning::

    The multiplayer feature is still experimental - you might experience more uncovered issues.

.. raw:: html

    <div style="position: relative; height: 0; overflow: hidden; max-width: 100%; height: auto; margin-bottom: 3em;">
    <video width="100%" height="auto" controls>
    <source src="_static/videos/multiplayer_example.webm" type="video/webm">
    Your browser does not support the video tag.
    </video>    
    </div>

