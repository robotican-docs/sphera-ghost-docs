# Sphera Demo Docs Reop


Online docs url:  https://sphera-ghost-docs.readthedocs.io/en/latest/


## Read the Docs

https://readthedocs.org/projects/sphera-ghost-docs/



## Setup

```
pip3 install myst-parser
pip3 install sphinx
pip3 install sphinx-rtd-theme
pip3 install sphinxcontrib-video
pip3 install sphinx-copybutton

mkdir docs
cd docs
sphinx-quickstart


```

## Local build

```
cd docs

make html
```