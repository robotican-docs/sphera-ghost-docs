 
======================
View RGB cameras Demo
======================

.. contents:: Table of Contents
    :local:



Setup
-------

#. Make sure you have installed **ghost_sphera_examples** and that its ROS2 WS is sourced.
#. verify by running:

  .. code-block:: console

      ros2 pkg prefix ghost_sphera_examples

if the output is *Package not found* - go back **installation** page and make sure you've completed *Installing Ghost Sphera Examples ROS2 Package*.

Run example 
------------
1. in order to read the video streams directly from Spera, open a new terminal and run the following command:

  .. code-block:: console

      ros2 launch ghost_sphera_examples rgb_cameras_example.launch.py

2. for also converting the streams into ROS2 image topics (sensor_msgs/msg/Image):

  .. code-block:: console

      ros2 launch ghost_sphera_examples rgb_cameras_example.launch.py publish_topics:=true

  .. note::

    the second option increase computation consumption and latency.

* here we use a default robot name: 'G1' and the default camera ports 5001 & 5005.
* the launch file contains: running Sphera simulation, spawning of one Vision60 robot, and 2 TCP clients for reading the GStreamer video streams of front_right_camera & rear_camera_link.
* in the example we show only two of the 5 robot's cameras. see the ports table below to add other cameras. you could also change those ports in the YAML file **spawn_vision60.yaml** located in the **ghost_sphera_examples** package.  
* you may choose to take **rgb_cameras_example.launch.py** from the **ghost_sphera_examples** package and adjust it for his needs.

.. raw:: html

    <div style="position: relative; height: 0; overflow: hidden; max-width: 100%; height: auto; margin-bottom: 3em;">
    <video width="100%" height="auto" controls>
    <source src="_static/videos/grb_cam_demo.webm" type="video/webm">
    Your browser does not support the video tag.
    </video>    
    </div>

Code Overview
--------------

Full launch file
``````````````````
.. code-block:: python

    #!/usr/bin/env python3

    import os
    import getpass
    from launch import LaunchDescription
    from launch_ros.actions import Node
    from ament_index_python.packages import get_package_share_directory
    from launch.actions import  ExecuteProcess, DeclareLaunchArgument
    from launch.conditions import IfCondition, UnlessCondition
    from launch.substitutions import LaunchConfiguration
                                
    def generate_launch_description():
        vision60_params = os.path.join(get_package_share_directory('ghost_sphera_examples'),
                                        'param', 'spawn_vision60.yaml')
        user = DeclareLaunchArgument('user', default_value=getpass.getuser().replace('-','_'))
        run_sphera = DeclareLaunchArgument('run_sphera', default_value='true')
        publish_topics = DeclareLaunchArgument('publish_topics', default_value='false')

        sphera = ExecuteProcess(
            cmd=['sphera -user=', LaunchConfiguration("user"), ' -map=Flat -origin=31.363789,34.877132,442.5'],
            shell=True,
            condition=IfCondition(LaunchConfiguration('run_sphera'))
        )
        spawn_vision60 =  Node(
            package='ghost_sphera_examples',
            executable='vision60_spawner_node.py',
            name='vision60_spawner_node',
            output='screen',
            parameters=[vision60_params,
                        {
                            'user': LaunchConfiguration('user'),
                            "front_right_camera.enable": True,
                            "rear_camera.enable": True,
                        }],
        )

        view_front_right_camera = ExecuteProcess(
            cmd=[['sleep 5 && gst-launch-1.0 tcpclientsrc host=127.0.0.1 port=5001 ! gdpdepay ! videoconvert ! autovideosink sync=false'
            ]],
            shell=True,
            condition=UnlessCondition(LaunchConfiguration('publish_topics'))
        )
        view_rear_camera = ExecuteProcess(
            cmd=[['sleep 5 && gst-launch-1.0 tcpclientsrc host=127.0.0.1 port=5005 ! gdpdepay ! videoconvert ! autovideosink sync=false'
            ]],
            shell=True,
            condition=UnlessCondition(LaunchConfiguration('publish_topics'))
        )


        front_right_camera_gscam_node = ExecuteProcess(
            cmd=[[f'sleep 5 && GSCAM_CONFIG="tcpclientsrc host=127.0.0.1 port=5001 ! gdpdepay ! videoconvert" ros2 run gscam  gscam_node --ros-args --remap /camera/image_raw:=/G1/front_right_camera/image_raw']],
            shell=True,
            condition=IfCondition(LaunchConfiguration('publish_topics'))
        )
        front_right_camera_image_view = Node(
            package='image_view',
            executable='image_view',
            name='front_right_camera_image_view',
            output='screen',
            parameters=[{'autosize': True}],
            remappings=[
                ('image', '/G1/front_right_camera/image_raw'),
            ],
            condition=IfCondition(LaunchConfiguration('publish_topics'))
        )

        rear_camera_gscam_node = ExecuteProcess(
            cmd=[[f'sleep 5 && GSCAM_CONFIG="tcpclientsrc host=127.0.0.1 port=5005 ! gdpdepay ! videoconvert" ros2 run gscam  gscam_node --ros-args --remap /camera/image_raw:=/G1/rear_camera/image_raw']],
            shell=True,
            condition=IfCondition(LaunchConfiguration('publish_topics'))
        )
        rear_camera_image_view = Node(
            package='image_view',
            executable='image_view',
            name='front_right_camera_image_view',
            output='screen',
            parameters=[{'autosize': True}],
            remappings=[
                ('image', '/G1/rear_camera/image_raw'),
            ],
            condition=IfCondition(LaunchConfiguration('publish_topics'))
        )


        return LaunchDescription([
            user,
            run_sphera,
            sphera,
            publish_topics,
            spawn_vision60,
            front_right_camera_gscam_node,
            front_right_camera_image_view,
            rear_camera_gscam_node,
            rear_camera_image_view,
            view_front_right_camera,
            view_rear_camera,
        ])



Examine the code
````````````````````
| let's in break down:
| first we import all needed packages:

.. code-block:: python

    #!/usr/bin/env python3

    import os
    import getpass
    from launch import LaunchDescription
    from launch_ros.actions import Node
    from ament_index_python.packages import get_package_share_directory
    from launch.actions import  ExecuteProcess, DeclareLaunchArgument
    from launch.conditions import IfCondition, UnlessCondition
    from launch.substitutions import LaunchConfiguration

| next we set *vision60_params* to get the parameters required for spawning the vision60 robot, *run_sphera* is set to true for executing the simulation directly from this launch file
| *user* is for the sphera sim user name, and *publish_topics* would be the command line arg to determine if we should convert the streams into Image topics

.. code-block:: python

    def generate_launch_description():
        vision60_params = os.path.join(get_package_share_directory('ghost_sphera_examples'),
                                        'param', 'spawn_vision60.yaml')
        user = DeclareLaunchArgument('user', default_value=getpass.getuser().replace('-','_'))
        run_sphera = DeclareLaunchArgument('run_sphera', default_value='true')
        publish_topics = DeclareLaunchArgument('publish_topics', default_value='false')

    | here we run the Sphera simulation with given parameters, and spawn a vision60 robot with two enabled cameras we want to use.

.. code-block:: python
    
        sphera = ExecuteProcess(
            cmd=['sphera -user=', LaunchConfiguration("user"), ' -map=Flat -origin=31.363789,34.877132,442.5'],
            shell=True,
            condition=IfCondition(LaunchConfiguration('run_sphera'))
        )
        spawn_vision60 =  Node(
            package='ghost_sphera_examples',
            executable='vision60_spawner_node.py',
            name='vision60_spawner_node',
            output='screen',
            parameters=[vision60_params,
                        {
                            'user': LaunchConfiguration('user'),
                            "front_right_camera.enable": True,
                            "rear_camera.enable": True,
                        }],
        )

| unless we set *publish_topics:=true*, a GStreamer pipline would be executed for each of the chosen cameras to be viewed on screen.

.. code-block:: python

    view_front_right_camera = ExecuteProcess(
        cmd=[['sleep 5 && gst-launch-1.0 tcpclientsrc host=127.0.0.1 port=5001 ! gdpdepay ! videoconvert ! autovideosink sync=false'
        ]],
        shell=True,
        condition=UnlessCondition(LaunchConfiguration('publish_topics'))
    )
    view_rear_camera = ExecuteProcess(
        cmd=[['sleep 5 && gst-launch-1.0 tcpclientsrc host=127.0.0.1 port=5005 ! gdpdepay ! videoconvert ! autovideosink sync=false'
        ]],
        shell=True,
        condition=UnlessCondition(LaunchConfiguration('publish_topics'))
    )

| if we set *publish_topics:=true*, the `gscam  <https://github.com/ros-drivers/gscam/tree/ros2>`_ nodes would be used to read the video streams and convert them into ROS2 Image topics.
| then the `image view  <https://github.com/ros-perception/image_pipeline/tree/foxy/image_view>`_ nodes would be used to read and display the Image messages on screen.

.. code-block:: python

    front_right_camera_gscam_node = ExecuteProcess(
        cmd=[[f'sleep 5 && GSCAM_CONFIG="tcpclientsrc host=127.0.0.1 port=5001 ! gdpdepay ! videoconvert" ros2 run gscam  gscam_node --ros-args --remap /camera/image_raw:=/G1/front_right_camera/image_raw']],
        shell=True,
        condition=IfCondition(LaunchConfiguration('publish_topics'))
    )
    front_right_camera_image_view = Node(
        package='image_view',
        executable='image_view',
        name='front_right_camera_image_view',
        output='screen',
        parameters=[{'autosize': True}],
        remappings=[
            ('image', '/G1/front_right_camera/image_raw'),
        ],
        condition=IfCondition(LaunchConfiguration('publish_topics'))
    )

    rear_camera_gscam_node = ExecuteProcess(
        cmd=[[f'sleep 5 && GSCAM_CONFIG="tcpclientsrc host=127.0.0.1 port=5005 ! gdpdepay ! videoconvert" ros2 run gscam  gscam_node --ros-args --remap /camera/image_raw:=/G1/rear_camera/image_raw']],
        shell=True,
        condition=IfCondition(LaunchConfiguration('publish_topics'))
    )
    rear_camera_image_view = Node(
        package='image_view',
        executable='image_view',
        name='front_right_camera_image_view',
        output='screen',
        parameters=[{'autosize': True}],
        remappings=[
            ('image', '/G1/rear_camera/image_raw'),
        ],
        condition=IfCondition(LaunchConfiguration('publish_topics'))
    )

View streams directly from command line 
-----------------------------------------

open a new terminal for each camera you want to view and run the following command:

    .. code-block:: console

         gst-launch-1.0 tcpclientsrc host=127.0.0.1 port=<desired camera port> ! gdpdepay ! videoconvert ! autovideosink sync=false


.. list-table:: Default Ports
   :widths: 1 1 
   :header-rows: 1

   * - camera
     - port
   * - front_right_camera
     - 5001
   * - front_left_camera_link
     - 5002
   * - left_camera_link
     - 5003
   * - right_camera_link
     - 5004
   * - rear_camera_link
     - 5005


Multiple Robots
-----------------
Spawn another robot with different YAML parameters and view its streams the same way.

  .. warning::
    | All ports across all of the robots in a simulation must be unique!
    | Make sure to set the ports if you spawn multiple robots.
    | Also make sure you are not using a port that is taken by another program.