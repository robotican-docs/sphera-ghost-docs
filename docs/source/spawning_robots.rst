 
================
Spawning Robots
================

.. contents:: Table of Contents
    :local:

Setup
-------

#. Make sure you have installed **ghost_sphera_examples** and that its ROS2 WS is sourced.
#. verify by running:

  .. code-block:: console

      ros2 pkg prefix ghost_sphera_examples

if the output is *Package not found* - go back **installation** page and make sure you've completed *Installing Ghost Sphera Examples ROS2 Package*.

Quick start 
----------------------
#. spawn a Vision60 robot and a person by running:
  .. code-block:: console

      ros2 launch ghost_sphera_examples spawn_example.launch.py

.. image:: images/spawn_example.png
   :align: center
   :width: 500

* for more complex setup with multiple spawns see the description below. you may want to modify this example script.

Spawn a Vision60 robot
`````````````````````````
1. create a new **.yaml** file and fill it with this contant:
  .. note::
    this example include all options for Vision60. one may choose to only modify a subset of them to keep the file short. 
  .. code-block:: yaml

    /**: 
      ros__parameters:
        user: ''
        name: "G1"
        location:
          x: 0.0
          y: 2.0
          z: 0.3
          yaw: 0.0 # In radians, 0=East, pi/2=North
        possess_on_spawn: False
        frame_id: "base_link"
        publish_map_to_base_link_tf: True

        max_walk_linear_speed: 3.0
        max_walk_angular_speed: 72.0
        max_acceleration: 4.0
        max_step_height: 0.45
        max_walkable_angle: 80.0

        lidar:
          enable: False
          debug: False
          frame_id: "lidar_link"
          topic: "~/points"
          rate: 5.0
          range_min: 0.5
          range_max: 100.0
          hfov_min: -180.0
          hfov_max: 180.0
          hfov_resolution: 0.3515625 # 360 / 1024
          vfov_min: -22.5 #-15.0
          vfov_max: 22.5 #15.0
          vfov_layers: 32 #16
          position: [0.3, 0.0, 0.5]
          add_ring_and_time: false
          yaw_alignment: 180.0 

        imu:
          enable: True
          frame_id: "imu_link"
          rate: 0.0

        gps:
          enable: True
          frame_id: "gps_link"
          rate: 10.0


        front_right_camera:
          enable: False
          frame_id: "front_right_camera_link"
          port: 5001
          hfov: 90.0
          width: 1280
          height: 720
          fps: 30.0
          gamma: 2.2
          render_method: 0 #0-2

        front_left_camera:
          enable: False
          frame_id: "front_left_camera_link"
          port: 5002
          hfov: 90.0
          width: 1280
          height: 720
          fps: 30.0
          gamma: 2.2
          render_method: 0 #0-2
        
        left_camera:
          enable: False
          frame_id: "left_camera_link"
          port: 5003
          hfov: 90.0
          width: 1280
          height: 720
          fps: 30.0
          gamma: 2.2
          render_method: 0 #0-2

        right_camera:
          enable: False
          frame_id: "right_camera_link"
          port: 5004
          hfov: 90.0
          width: 1280
          height: 720
          fps: 30.0
          gamma: 2.2
          render_method: 0 #0-2

        rear_camera:
          enable: False
          frame_id: "rear_camera_link"
          port: 5005
          hfov: 90.0
          width: 1280
          height: 720
          fps: 30.0
          gamma: 2.2
          render_method: 0 #0-2


        front_depth_camera:
          enable: False
          frame_id: "front_depth_camera_link"
          port: 5011
          hfov: 90.0
          width: 640
          height: 360
          fps: 10.0

        rear_depth_camera:
          enable: False
          frame_id: "rear_depth_camera_link"
          port: 5012
          hfov: 90.0
          width: 640
          height: 360
          fps: 10.0

2. while the simulation is running, execute the following in a terminal:
    .. code-block:: console

        ros2 run ghost_sphera_examples vision60_spawner_node.py --ros-args --params-file <path/to/your/file.yaml>

Spawn a Person
`````````````````````````
1. create a new **.yaml** file and fill it with this contant:
  .. note::
    this example include all options for Person.
  .. code-block:: yaml

      /**: 
        ros__parameters:
          user: ''
          name: "P1"
          location:
            x: 1.0
            y: 2.0
            z: 0.3
            yaw: 0.0
          possess_on_spawn: False
          speed: 3.0

          ai_navigation:
            type: ""  # "RandomInRadius" \ "Waypoints" \ "" (no ai nav)
            loop: True
            radius: 10.0 
            acceptance_radius: 1.0
            waypoints_file: "" # default:  "" (waypoints.json from this package)

2. a person can be spawned with AI navigaton options.
  | "RandomInRadius" - for randomly moving within a specified radius
  | and "Waypoints" - for moving from one point to another n order.
  | the waypoints need to be specified in a seperate .json file with the following structure:

  .. code-block:: json

      {
        "waypoint_0": {
          "x": 11.947,
          "y": 0.670,
          "z": 0.0,
          "speed": 5.0
        },
        "waypoint_1": {
          "x": 18.979,
          "y": 0.655,
          "z": 0.0,
          "speed": 5.0
        },
        "waypoint_2": {
          "x": 18.009,
          "y": 7.731,
          "z": 0.0,
          "speed": 5.0
        },
        "waypoint_3": {
          "x": 12.015,
          "y": 7.695,
          "z": 0.0,
          "speed": 5.0
        }
      } 


3. while the simulation is running, execute the following in a terminal:
    .. code-block:: console

        ros2 run ghost_sphera_examples person_spawner_node.py --ros-args --params-file <path/to/your/file.yaml>

Common spawn options 
----------------------

each spawnable entity have multiple options that can be set from a YAML file.
for each spawn instance, different options can be used.


.. list-table:: Common Spawn Options
   :widths: 1 1 1 1
   :header-rows: 1

   * - Option
     - Notes
     - Range
     - Default
   * - user 
     - the name of sphera simulation user to spawn in
     - string
     - '' (empty string means the taking the computer's user name)
   * - name 
     - the name of the spawned entity
     - string
     - 'G1'
   * - possess_on_spawn
     - Auto possess in Sphera after spawn
     - true \\ false
     - false
   * - location.x 
     - robot location in the X axis (meters)
     - any number
     - 0.0
   * - location.x 
     - robot location in the X axis (meters)
     - any number
     - 0.0
   * - location.y 
     - robot location in the Y axis (meters)
     - any number
     - 2.0
   * - location.z 
     - robot location in the Z axis (meters)
     - any number
     - 0.0
   * - location.yaw 
     - robot orientation in the Z axis (radians)
     - -pi - pi
     - 0.0


Sensors parameters spawn options
----------------------------------
the Vision60 robot has multiple sensors - IMU, GPS, RGB Cameras, Depth cameras, Multi-layer LiDAR.

RGB Camera Spawn Options
`````````````````````````

.. list-table:: RGB Camera Spawn Options
   :widths: 1 1 1 1
   :header-rows: 1

   * - Option
     - Notes
     - Range
     - Default
   * - <sensor_name>.enable
     - if this sensor may be used
     - true \\ false
     - false 
   * - <sensor_name>.frame_id
     - the TF frame of the sensor
     - string
     - <sensor_name>_link
   * - <sensor_name>.port
     - Port used for gstreamer TCP server
     - any
     - 0 = disable streaming
   * - <sensor_name>.hfov
     - Camera Horizontal field of view in degrees
     - 0.1- 170.0
     - 90.0
   * - <sensor_name>.fps
     - Camera framerate in Hz
     - 0.1 - 60.0
     - 30
   * - <sensor_name>.width
     - Camera resolution width in pixels
     - 1 - 3840
     - 1280
   * - <sensor_name>.height
     - Camera resolution height in pixels
     - 1 - 2160
     - 720
   * - <sensor_name>.gamma
     - Camera gamma value
     - 0.1 - 50
     - 2.2
   * - <sensor_name>.render_method
     - Camera render method. 1 & 2 would require more computation and include auto exposure
     - 0, 1, or 2
     - 0

Depth Camera Spawn Options
```````````````````````````
.. list-table:: Depth Camera Spawn Options
   :widths: 1 1 1 1
   :header-rows: 1

   * - Option
     - Notes
     - Range
     - Default
   * - <sensor_name>.enable
     - if this sensor may be used
     - true \\ false
     - false 
   * - <sensor_name>.frame_id
     - the TF frame of the sensor
     - string
     - <sensor_name>_link
   * - <sensor_name>.port
     - Port used for gstreamer TCP server
     - any
     - 0 = disable streaming
   * - <sensor_name>.hfov
     - Camera Horizontal field of view in degrees
     - 0.1- 170.0
     - 90.0
   * - <sensor_name>.fps
     - Camera framerate in Hz
     - 0.1 - 60.0
     - 10.0
   * - <sensor_name>.width
     - Camera resolution width in pixels
     - 1 - 3840
     - 640
   * - <sensor_name>.height
     - Camera resolution height in pixels
     - 1 - 2160
     - 480
   

3D (Multi Layer) LIDAR Spawn Options
```````````````````````````````````````

.. list-table:: 3D LIDAR Spawn Options
   :widths: 1 1 1 1
   :header-rows: 1

   * - Option
     - Notes
     - Range
     - Default
   * - <sensor_name>.enable
     - if this sensor may be used
     - true \\ false
     - false 
   * - <sensor_name>.debug
     - if this sensor rays should be displayed in the simulation (would slow runtime dramatically)
     - true \\ false
     - false 
   * - <sensor_name>.frame_id
     - the TF frame of the sensor
     - string
     - "lidar_link"
   * - <sensor_name>.topic
     - Published topic name
     - valid topic name
     - ~/points
   * - <sensor_name>.rate
     - Published readings rate in Hz
     - 0.1 - 100.0
     - 5.0
   * - <sensor_name>.range_min
     - Sensor minimum range in meters
     - 0.01 - 1000.0
     - 0.5
   * - <sensor_name>.range_max
     - Sensor maximum range in meters
     - 0.01 - 1000.0
     - 100.0
   * - <sensor_name>.hfov_min
     - Sensor minimum horizontal angle in degrees, clockwise positive
     - -180.0 - 180.0
     - -60.0
   * - <sensor_name>.hfov_max
     - Sensor maximum horizontal angle in degrees, clockwise positive
     - -180.0 - 180.0
     - 60.0
   * - <sensor_name>.hfov_resolution
     - Sensor horizontal angular resolution in degrees
     - 0.01 - 180.0
     - 0.3
   * - <sensor_name>.vfov_min
     - Sensor minimum vertical angle in degrees, upward positive
     - -90.0 - 90.0
     - -22.5
   * - <sensor_name>.vfov_max
     - Sensor maximum vertical angle in degrees, upward positive
     - -90.0 - 90.0
     - 22.5
   * - <sensor_name>.vfov_layers
     - Number of vertical layers
     - 2 - 50
     - 32
   * - <sensor_name>.position
     - sensor's frame offset from the base frame
     - [x_offset, y_offset, z_offset]
     - [0.3, 0.0, 0.5]
   * - <sensor_name>.add_ring_and_time
     - | if the the point cloud data would include the 'time' and 'ring' fields - so each point in data would be of data type XYZIRT.
       | otherwise, each point would be of data type XYZI. (it is recommended to use XYZI for better performance if 'time' and 'ring' fields are not needed)
     - true \\ false
     - false
   * - <sensor_name>.yaw_alignment
     - the yaw angle offset from the sensor's forwards direction, where the first point in the cloud would be measured
     - -180.0 - 180.0
     - 180.0

.. warning::
  | Setting ``<sensor_name>.add_ring_and_time: true`` is more computationally intensive. Therefore, it is recommended to set only if used.


GPS Spawn Options
`````````````````````````
.. list-table:: GPS Options
   :widths: 1 1 1 1
   :header-rows: 1

   * - Option
     - Notes
     - Range
     - Default
   * - <sensor_name>.enable
     - if this sensor may be used
     - true \\ false
     - false 
   * - <sensor_name>.frame_id
     - the TF frame of the sensor
     - string
     - gps_link
   * - <sensor_name>.rate
     - the data publish rate.
     - greater than 0.0
     - 10.0

IMU Spawn Options
`````````````````````````
.. list-table:: IMU Options
   :widths: 1 1 1 1
   :header-rows: 1

   * - Option
     - Notes
     - Range
     - Default
   * - <sensor_name>.enable
     - if this sensor may be used
     - true \\ false
     - false 
   * - <sensor_name>.frame_id
     - the TF frame of the sensor
     - string
     - imu_link
   * - <sensor_name>.rate
     - the data publish rate. 0.0 - max rate.
     - any
     - 0.0

Vision60
----------

.. image:: images/vision60.png
   :align: center
   :width: 500


Vision60 sensors
`````````````````````````````

.. list-table:: Vision60 Sensors
   :widths: 1 1 1
   :header-rows: 1

   * - Sensor
     - Name
     - Notes

   * - Day Cameras
     - camera
     - 2 in the front, 1 on each side, 1 in the rear.

   * - 3D LIDAR
     - lidar 
     - placed on top of the robot, usually used for SLAM and other autonomy algorithms.

   * - Depth cameras
     - depth_camera
     - 1 in the front and 1 in the rear, both tilted downwards to see the ground.

   * - GPS
     - gps
     - used to track global location (the initial location can be configured in the menu). the topic would be /<robot_name>/gx5/gnss1/fix

   * - IMU
     - imu
     - Inertial measurement unit. the topic would be /<robot_name>/mcu/state/imu


Vision60 Specific Spawn options
`````````````````````````````````

.. list-table:: 
   :widths: 1 1 1 1
   :header-rows: 1

   * - Option
     - Notes
     - Range
     - Default
   * - frame_id
     - Set the robot's base link id
     - String
     - "base_link"
   * - publish_map_to_base_link_tf
     - Enable publishing ground truth Odometry as part of the TF tree
     - true \\ false
     - true
   * - max_walk_linear_speed
     - maximum linear speed of the robot (m/s)
     - 0.01 - 100.0
     - 3.0
   * - max_walk_angular_speed
     - maximum angular speed of the robot (deg/s)
     - 1.0 - 360.0
     - 72.0
   * - max_acceleration
     - walking acceleration of the robot (m/s^2)
     - 0.1 - 100.0
     - 4.0
   * - max_step_height
     - maximum height gap the robot would be able to walk through (meters)
     - 0.01 - 1.0
     - 0.45
   * - max_walkable_angle
     - maximum slope angle the robot would be able to walk through (deg)
     - 1.0 - 89.0
     - 80.0


.. warning::
  | Adjust movement parameters (``max_walk_linear_speed``, ``max_walk_angular_speed``, ``max_acceleration``, ``max_step_height``, ``max_walkable_angle``) with caution!
  | Extreme values may cause unexpected behaviour.
  

Person
----------

.. image:: images/person.png
   :align: center
   :width: 500


Person Specific Spawn options
`````````````````````````````````

.. list-table::
   :widths: 1 1 1 1
   :header-rows: 1

   * - Option
     - Notes
     - Range
     - Default
   * - speed
     - walking speed m/s
     - 0.1 - 100.0
     - 3.0
   * - ai_navigation.type
     - the type of AI navigation to use for this spawned person
     - "RandomInRadius" \ "Waypoints" \ "" (no ai nav)
     - ""
   * - ai_navigation.loop
     - if true and ai_navigation.type is "Waypoints" then the person will keep going between points in a loop
     - True / False
     - True
   * - ai_navigation.radius
     - the radius for "RandomInRadius" (meters)
     - float
     - 10.0
   * - ai_navigation.acceptance_radius
     - the acceptance radius for points locations in "Waypoints" (meters)
     - float
     - 1.0
   * - ai_navigation.waypoints_file
     - the path to a json file of waypoints for "Waypoints"
     - string
     - "" (take waypoints.json from ghost_sphera_examples package)

Waypoint JSON structure
`````````````````````````
* each waypoint has a location in the level (x,y,z) and speed for the person to move in.
* if a waypoint is unreachable, then it would be skipped.


  .. code-block:: json

      {
        "waypoint_0": {
          "x": float,
          "y": float,
          "z": float,
          "speed": float
        },
        "waypoint_1": {
          "x": float,
          "y": float,
          "z": float,
          "speed": float
        },
        "waypoint_2": {
          "x": float,
          "y": float,
          "z": float,
          "speed": float
        },

        ...

        "waypoint_n": {
          "x": float,
          "y": float,
          "z": float,
          "speed": float
        }
      } 