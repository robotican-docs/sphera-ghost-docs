 
============================
LiDAR & Depth Cameras Demo
============================

.. contents:: Table of Contents
    :local:



Setup
-------

#. Make sure you have installed **ghost_sphera_examples** and that its ROS2 WS is sourced.
#. verify by running:

  .. code-block:: console

      ros2 pkg prefix ghost_sphera_examples

if the output is *Package not found* - go back **installation** page and make sure you've completed *Installing Ghost Sphera Examples ROS2 Package*.

Run example
------------
open a new terminal and run the following command (after setup stages)

  .. code-block:: console

      ros2 launch ghost_sphera_examples depth_and_lidar_example.launch.py


* here we use a default robot name: 'G1' and the default depth camera ports 5011 & 5012.
* the launch file contains: running Sphera simulation, spawning of one Vision60 robot, running 2 depth cameras, and opening the rviz2 GUI to show the LiDAR & Depth cameras readings.
* one may choose to take **depth_and_lidar_example.launch.py** from the **ghost_sphera_examples** package and adjust it for his needs.

.. raw:: html

    <div style="position: relative; height: 0; overflow: hidden; max-width: 100%; height: auto; margin-bottom: 3em;">
    <video width="100%" height="auto" controls>
    <source src="_static/videos/depth_and_lidar_demo.webm" type="video/webm">
    Your browser does not support the video tag.
    </video>    
    </div>

.. list-table:: default ports
   :widths: 1 1
   :header-rows: 1
   * - camera
     - port
   * - front_depth_camera
     - 5011
   * - rear_depth_camera
     - 5012

Code Overview
--------------

Full launch file
``````````````````
.. code-block:: python

  #!/usr/bin/env python3

  import os
  import getpass
  from launch import LaunchDescription
  from launch_ros.actions import Node
  from ament_index_python.packages import get_package_share_directory
  from launch.actions import  ExecuteProcess, DeclareLaunchArgument
  from launch.conditions import IfCondition
  from launch.substitutions import LaunchConfiguration

  def generate_launch_description():
      vision60_params = os.path.join(get_package_share_directory('ghost_sphera_examples'),
                                      'param', 'spawn_vision60.yaml')
      rviz_config = os.path.join(get_package_share_directory('ghost_sphera_examples'), 'rviz','ghost_sensors.rviz')
      user = DeclareLaunchArgument('user', default_value=getpass.getuser().replace('-','_'))
      run_sphera = DeclareLaunchArgument('run_sphera', default_value='true')

      spawn_vision60 =  Node(
          package='ghost_sphera_examples',
          executable='vision60_spawner_node.py',
          name='vision60_spawner_node',
          output='screen',
          parameters=[vision60_params,
                      {
                          'user': LaunchConfiguration('user'),
                          'lidar.enable': True,
                          "front_depth_camera.enable": True,
                          "rear_depth_camera.enable": True,
                      }],
      )
      sphera = ExecuteProcess(
          cmd=['sphera -user=', LaunchConfiguration("user"), ' -map=Flat -origin=31.363789,34.877132,442.5'],
          shell=True,
          condition=IfCondition(LaunchConfiguration('run_sphera'))
      )
      front_depth_camera =  Node(
          package='sphera_vision_tools',
          executable='depth_camera_proxy_node',
          name='front_depth_camera_proxy',
          output='screen',
          parameters=[{
              "depth_camera_proxy.pipeline": "tcpclientsrc host=127.0.0.1 port=5011 ! gdpdepay ! appsink sync=false max-buffers=1 drop=true name=appsink",
              "depth_camera_proxy.frame_id": "front_depth_camera_link",
              "depth_camera_proxy.topic": "/G1/front_depth_camera_points",
              "depth_camera_proxy.min_depth": 0.1,
              "depth_camera_proxy.max_depth": 10.0,
          }],
      )
      rear_depth_camera =  Node(
          package='sphera_vision_tools',
          executable='depth_camera_proxy_node',
          name='rear_depth_camera_proxy',
          output='screen',
          parameters=[{
              "depth_camera_proxy.pipeline": "tcpclientsrc host=127.0.0.1 port=5012 ! gdpdepay ! appsink sync=false max-buffers=1 drop=true name=appsink",
              "depth_camera_proxy.frame_id": "rear_depth_camera_link",
              "depth_camera_proxy.topic": "/G1/rear_depth_camera_points",
              "depth_camera_proxy.min_depth": 0.1,
              "depth_camera_proxy.max_depth": 10.0,
          }],
      )


      rviz = Node(package="rviz2", executable="rviz2",
                  namespace="G1",
                  arguments=['-d', rviz_config],
                  output="screen",
                  remappings=[('/tf', '/G1/tf')],
                  )

      return LaunchDescription([
          user,
          run_sphera,
          sphera,
          spawn_vision60,
          front_depth_camera,
          rear_depth_camera,
          rviz
      ])

Examine the code
````````````````````
| let's in break down:
| first we import all needed packages:

.. code-block:: python

  #!/usr/bin/env python3

  import os
  import getpass
  from launch import LaunchDescription
  from launch_ros.actions import Node
  from ament_index_python.packages import get_package_share_directory
  from launch.actions import  ExecuteProcess, DeclareLaunchArgument
  from launch.conditions import IfCondition
  from launch.substitutions import LaunchConfiguration

| next we set *vision60_params* to get the parameters required for spawning the vision60 robot, *run_sphera* is set to true for executing the simulation directly from this launch file
| *user* is for the sphera sim user name, and *publish_topics* would be the command line arg to determine if we should convert the streams into Image topics

.. code-block:: python

  def generate_launch_description():
      vision60_params = os.path.join(get_package_share_directory('ghost_sphera_examples'),
                                      'param', 'spawn_vision60.yaml')
      rviz_config = os.path.join(get_package_share_directory('ghost_sphera_examples'), 'rviz','ghost_sensors.rviz')
      user = DeclareLaunchArgument('user', default_value=getpass.getuser().replace('-','_'))
      run_sphera = DeclareLaunchArgument('run_sphera', default_value='true')

| here we run the Sphera simulation with given parameters, and spawn a vision60 robot with both depth cameras enabled so we would be able to use them.

.. code-block:: python
    
      spawn_vision60 =  Node(
          package='ghost_sphera_examples',
          executable='vision60_spawner_node.py',
          name='vision60_spawner_node',
          output='screen',
          parameters=[vision60_params,
                      {
                          'user': LaunchConfiguration('user'),
                          'lidar.enable': True,
                          "front_depth_camera.enable": True,
                          "rear_depth_camera.enable": True,
                      }],
      )
      sphera = ExecuteProcess(
          cmd=['sphera -user=', LaunchConfiguration("user"), ' -map=Flat -origin=31.363789,34.877132,442.5'],
          shell=True,
          condition=IfCondition(LaunchConfiguration('run_sphera'))
      )

| here we run 2 instances of *depth_camera_proxy_node* from our custom included package *sphera_vision_tools*.
| this node reads the depth data using GStreamer, parse it into a *sensor_msgs/msg/PointCloud2*, and publish using ROS2.

.. code-block:: python

      front_depth_camera =  Node(
          package='sphera_vision_tools',
          executable='depth_camera_proxy_node',
          name='front_depth_camera_proxy',
          output='screen',
          parameters=[{
              "depth_camera_proxy.pipeline": "tcpclientsrc host=127.0.0.1 port=5011 ! gdpdepay ! appsink sync=false max-buffers=1 drop=true name=appsink",
              "depth_camera_proxy.frame_id": "front_depth_camera_link",
              "depth_camera_proxy.topic": "/G1/front_depth_camera_points",
              "depth_camera_proxy.min_depth": 0.1,
              "depth_camera_proxy.max_depth": 10.0,
          }],
      )
      rear_depth_camera =  Node(
          package='sphera_vision_tools',
          executable='depth_camera_proxy_node',
          name='rear_depth_camera_proxy',
          output='screen',
          parameters=[{
              "depth_camera_proxy.pipeline": "tcpclientsrc host=127.0.0.1 port=5012 ! gdpdepay ! appsink sync=false max-buffers=1 drop=true name=appsink",
              "depth_camera_proxy.frame_id": "rear_depth_camera_link",
              "depth_camera_proxy.topic": "/G1/rear_depth_camera_points",
              "depth_camera_proxy.min_depth": 0.1,
              "depth_camera_proxy.max_depth": 10.0,
          }],
      )

Multiple Robots
-----------------
Spawn another robot with different YAML parameters and view its streams the same way.
  .. warning::
    | All ports across all of the robots in a simulation must be unique!
    | Make sure to set the ports if you spawn multiple robots.
    | Also make sure you are not using a port that is taken by another program.