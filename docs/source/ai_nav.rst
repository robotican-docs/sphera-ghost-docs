 
=====================
AI Person Navigation
=====================

.. contents:: Table of Contents
    :local:



Setup
-------

#. Make sure you have installed **ghost_sphera_examples** and that its ROS2 WS is sourced.
#. verify by running:

  .. code-block:: console

      ros2 pkg prefix ghost_sphera_examples

if the output is *Package not found* - go back **installation** page and make sure you've completed *Installing Ghost Sphera Examples ROS2 Package*.

Run example 
----------------
1. open a new terminal and run the following command:

  .. code-block:: console

      ros2 launch ghost_sphera_examples ai_nav_example.launch.py

you should see the spawend person running around the rainbow cube (if you did not change waypoints.json)

2. for random walk in radius open a new terminal and run the following command: 

  .. code-block:: console

      ros2 launch ghost_sphera_examples ai_nav_example.launch.py ai_type:="RandomInRadius"

you should see the spawend person running randomly within the specified radius



Setup your own AI 
--------------------------

Waypoints
``````````````````

1. create JSON file of the following structure:

  .. code-block:: json

      {
        "waypoint_0": {
          "x": float,
          "y": float,
          "z": float,
          "speed": float
        },
        "waypoint_1": {
          "x": float,
          "y": float,
          "z": float,
          "speed": float
        },
        "waypoint_2": {
          "x": float,
          "y": float,
          "z": float,
          "speed": float
        },

        ...

        "waypoint_n": {
          "x": float,
          "y": float,
          "z": float,
          "speed": float
        }
      } 

2. use the ``Object Spawning`` tool to help you get locations in the level
   | by choosing ``k``, pointing to desired waypoint location and clicking the ``mouse scroll wheel`` to copy location to clipboard.
   | the copied string would be of the following structure: ``X=float Y=float Z=float``.

.. image:: images/waypoint_click.png
   :align: center
   :width: 500

3. choose the speed for each waypoint
4. in the ``spawn_person.yaml`` file set the following parameters (or override them on execution):
   | ``ai_navigation.type``: "Waypoints"
   | ``ai_navigation.waypoints_file``: path to your JSON file
   | ``ai_navigation.loop``: if true, the person would keep walking from point to point in a loop
   | ``ai_navigation.acceptance_radius``: float - for each waypoint this is the radius to find a traversable point to walk to

Random walk in radius
````````````````````````

1. choose the spawning location - it would be the origin of the walk circle.
2.  in the ``spawn_person.yaml`` file set the following parameters (or override them on execution):
   | ``ai_navigation.type``: "RandomInRadius"
   | ``ai_navigation.radius``: if true, the person would keep walking from point to point in a loop. otherwise he would choose one random point.
   | ``ai_navigation.loop``: "RandomInRadius"


