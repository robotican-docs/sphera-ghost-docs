 
====================
ROS2 Interfaces
====================

.. contents:: Table of Contents
    :local:

General Simulation Interfaces
------------------------------

Published topics
```````````````````

.. list-table:: 
   :widths: 1 1 1 
   :header-rows: 1

   * - Topic
     - Type
     - description
   * - /<robot_name>/sphera/state
     - sphera_common_interfaces/msg/SpheraPawnState
     - ground truth 6 DOF location, and XYZ velocity for each spawned character
   * - /sphera/<user>/status
     - sphera_common_interfaces/msg/SpheraStatus
     - status for each Sphera simulation instance (ground truth)

Services
`````````````
.. list-table:: 
   :widths: 1 1 1
   :header-rows: 1
   
   * - Name
     - Type
     - description
   * - /sphera/<user_name>/set
     - sphera_common_interfaces/srv/SpheraSet
     - set environmental properties - weather, wind conditions & time of day (see weather types table below)
   * - /sphera/<user_name>/destroy
     - sphera_common_interfaces/srv/DestroyActor
     - This service is used to destroy robots by specifying their name.
   * - /sphera/<user_name>/spawn
     - sphera_common_interfaces/srv/SpawnActor
     - This service is used to spawn robots. see 'Spawning Robots' for more info.

.. list-table:: SpheraSet service request has one field - 'data' - that is parsed as JSON with the following format 
   :widths: 1 1 1
   :header-rows: 1

   * - field
     - values
     - description
   * - environment.time_of_day
     - "00:00:00"-"24:00:00" (string)
     - time of day in <hour>:<minute>:<second> format
   * - environment.weather
     - one of ["light rain","rain", "thunderstorm", "light snow", "snow", "blizzard", "dust storm", "dust", "foggy", "clear skies", "partly cloudy", "cloudy", "overcast"] (string)
     - a preset for weather conditions
   * - environment.wind_direction
     - 0-360 (integer)
     - the wind azimuth direction in degrees.
   * - environment.wind_speed
     - 0-15 (integer)
     - the wind speed in m/s.

* example call: ``ros2 service call /sphera/meir/set sphera_common_interfaces/srv/SpheraSet "{data: '{\"environment\": {\"time_of_day\": \"6:30:00\", \"weather\": \"dust\", \"wind_direction\": 90, \"wind_speed\": 15}}'}"``
* for a quick start with setting environment conditions see the 'Weather Change Demo' page.


Vision60
---------

Published topics
```````````````````

.. list-table:: 
   :widths: 1 1 1
   :header-rows: 1

   * - Topic
     - Type
     - description
   * - /<robot_name>/tf
     - tf2_msgs/msg/TFMessage
     - the transformations tree of the robot
   * - /<robot_name>/collision
     - std_msgs/msg/String
     - information about collision in JSON format
   * - /<robot_name>/points
     - sensor_msgs/msg/PointCloud2
     - the 3D LiDAR point cloud data
   * - /<robot_name>/imu
     - sensor_msgs/msg/Imu
     - the IMU data
   * - /<robot_name>/gps_fix
     - sensor_msgs/msg/NavSatFix
     - the GPS data - global location (level's origin LLA can be set in the main menu)
   * - /<robot_name>/sphera/set_state
     - sphera_common_interfaces/msg/SpheraPawnState
     - used for robot location reset, without the need to respawn. example usage from cli: ``ros2 topic pub /<robot_name>/sphera/set_state sphera_common_interfaces/msg/SpheraPawnState "{location: {x: 1.0, y: 2.0, z: 0.6}, rotation: {yaw: 90.0}}" --once``
   * - /<robot_name>/mcu/state/rel_pose
     - geometry_msgs/msg/PoseStamped
     - relative pose state of the robot - only Z, pitch and roll.
   * - /<robot_name>/mcu/state/vel
     - geometry_msgs/msg/TwistStamped
     - velocity state of the robot in body frame

.. note::

  | The ``/<robot_name>/mcu/state/rel_pose`` interface uses the orientation fields as normalized quaternion, with the yaw component always set to 0.
  | The given pose is relative to the plane created by the feet 3D placements, similar to the actual hardware robot.

Subscribed topics
```````````````````

.. list-table:: 
   :widths: 1 1 1
   :header-rows: 1

   * - Topic
     - Type
     - description
   * - /<robot_name>/mcu/command/manual_twist
     - geometry_msgs/msg/Twist
     - twist command - used for moving the robot while in walk mode
   * - /<robot_name>/mcu/command/pose
     - geometry_msgs/msg/Pose
     - pose command - used to set the robot's body pose while in stand mode
   * - /<robot_name>/command/setAction
     - std_msgs/msg/UInt32
     - change between Sit (data: 0), Stand (data: 1), Walk (data: 2)

.. note::

  | The ``/<robot_name>/mcu/command/pose`` interface uses the orientation fields as normalized euler degrees instead of true quaternion.
  | meaning: orientation.x is roll, orientation.y is pitch, orientation.z is yaw. orientation.w is not used.
  | each field is normalized from degree limits to [-1,1]. limit are roll [-14, 14], pitch [-16,16], yaw [-45,45]
  | In addition, control over the robot height (position.z) is also normalized from [0.41,0.5] to [-1,1]
  | This interface was made to match the real robot's interface.


Services
`````````````
.. list-table:: 
   :widths: 1 1 1
   :header-rows: 1

   * - Name
     - Type
     - description
   * - /<robot_name>/execute_headlights_off
     - std_srvs/srv/Empty
     - trun off robot's headlights
   * - /<robot_name>/execute_headlights_on
     - std_srvs/srv/Empty
     - trun on robot's headlights
   * - /<robot_name>/execute_taillights_off
     - std_srvs/srv/Empty
     - trun off robot's taillights
   * - /<robot_name>/execute_taillights_on
     - std_srvs/srv/Empty
     - trun on robot's taillights

TF Tree
`````````````

* The robot's transformations tree include transformations between *base_link* and each sensor's frame.
* If the robot was spawned with parameter *publish_map_to_base_link_tf: True*, the transformation *map <-> base_link* would be included. It gives the ground truth odometry of the robot.
* The tree, as visualized in the image below is published under the following topic: */<robot_name>/tf*.

.. image:: images/vision60_tf_tree.png
   :align: center
   :width: 800

Person
-----------

Subscribed topics
```````````````````

.. list-table:: 
   :widths: 1 1 1
   :header-rows: 1

   * - Topic
     - Type
     - description
   * - /<person_name>/cmd_vel
     - geometry_msgs/msg/Twist
     - make the person walk, following a given velocity vector