 
================
Getting Started
================

.. contents:: Table of Contents
    :local:
    
Launching Sphera
------------------

To launch Sphera from terminal run:
    
    .. code-block:: console

        sphera


To launch Sphera and host specific setup use the following syntax:
    
    .. code-block:: console

        sphera [-host=<user_name>] [-map=<level_name>] [-origin=<latitude>,<longitude>,<altitude_amsl>] [-RenderOffscreen] [-timeOfDay="<HH:MM>"]

    .. list-table:: available cli flags
      :widths: 1 1 1 1
      :header-rows: 1

      * - flag
        - value
        - default
        - description
      * - -host
        - string
        - ``$USER``
        - the namespace for controlling a sphera sim instance
      * - -map
        - string - must be in the list of available levels as seen in the main menu.
        - Flat
        - the level to be loaded
      * - -origin
        - string of structure <float>,<float>,<float> - must be in the list of available levels as seen in the main menu.
        - 31.282572,34.849328,332
        - the LLA coordinates to be set for the level's origin
      * - -RenderOffscreen
        - this is a boolean. if preset it's set to true.
        - false
        - if true - the GUI interface would not be opened. this can help save computation resources for automated runs.
      * - -timeOfDay
        - string of structure "<hour>:<minutes>" - must be valid time of day (24-hour time format)
        - "12:34"
        - the time of day to be set when level is loaded. (time of day can be set multiple time during runtime through the weather widget or using ROS2 interface)


for example:

    .. code-block:: console

        sphera -user=john -map=Flat -origin=31.363789,34.877132,442.5 -timeOfDay="07:30"

To Launch Sphera in headless mode add the -RenderOffscreen argument, e.g.
    
    .. code-block:: console

        sphera -user=john -map=Flat -origin=31.363789,34.877132,442.5  -timeOfDay="07:30" -RenderOffscreen


See Host menu section for more details.


It is also possible to launch Sphera from the grphical Application Launcher.


Menus
-------


Main menu
```````````

.. image:: images/mainMenu.png
   :align: center
   :width: 600

From the main menu you can set the user name that will be used for both hosting a new simulation and for joining an existing session.

The server name is used as identification name for multi user scenarios. Keep in mind that joining an existing session requires a unique user name.

Different levels (environments) can be chosen from the 'Level' list.

The geographic coordinates are used to set the absolute world position of the cartesian origin of the level.

Join menu
`````````````

.. image:: images/joinMenu.png
   :align: center
   :width: 600


Entering this menu trigger a scan for available servers running ont the same LAN.

Clicking on a server name will initiate joining the server.

Options menu
`````````````

.. image:: images/options_menu.png
   :align: center
   :width: 600


On the first run of Sphera, you need to set the path to your licence .bin file, as shown in the image above.

Additionally, Sphera volume level can be controled from here.

  .. note::

    The environment variable ``SPHERA_LICENSE_FILE`` would override license file absolute path if set.

Hot Keys
----------
After hosting ir joining a server, you can use the following keys for exploring, debugging ir manually controlling the simulation:

.. list-table:: Hot Keys
   :widths: 25 100
   :header-rows: 1

   * - Key
     - Function
   * - Escape
     - Quit Sphera
   * - Ctrl+q
     - Quit level (back to main menu)
   * - a,w,s,d
     - Camera view adjustment keys (can be used with mouse click and drag)
   * - o
     - Toggle Scalability widget (graphicas performance adjustments)
   * - t
     - Toggle Envirenment settings widget
   * - l
     - Toggle robots widget
   * - k
     - Toggle objects widget

Widgets
--------

Scalability Widget
`````````````````````

.. image:: images/scalability.png
   :align: center
   :width: 500

Changing the slider will result in a change in the quality of the graphic engine and a change in the processing resources consumed.


Envirenment Settings Widget
`````````````````````````````

.. raw:: html

    <div style="position: relative; height: 0; overflow: hidden; max-width: 100%; height: auto; margin-bottom: 3em;">
    <video width="100%" height="auto" controls>
    <source src="_static/videos/sphera_weather_demo.webm" type="video/webm">
    Your browser does not support the video tag.
    </video>    
    </div>

| Changing the Time of Day slider will change lightning conditions and sun position in the sky.
| Choosing an option from the dropdown list would initiate a preset weather condition.

Robots Widget
``````````````````

.. image:: images/robots.png
   :align: center
   :width: 500


.. list-table:: Robots Widgets options (By clicking on a robot name)
   :widths: 1 1
   :header-rows: 1

   * - Option
     - Notes
   * - Left click
     - posseess the robot in its current location.
   * - Right click
     - destroy the robot.


| Possessing a certain robot will result in the main camera tracking the robot and will also allow manual control if the robot allows such.
| Please note, if the robot allows manual control, it will not be possible to control it through the ROS2 interface and the manual interface at the same time.