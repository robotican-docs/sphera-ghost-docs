 
==========================
Robot Twist commands Demo
==========================

.. contents:: Table of Contents
    :local:


Setup
-------

#. Make sure you have installed **ghost_sphera_examples** and that its ROS2 WS is sourced.
#. verify by running:

  .. code-block:: console

      ros2 pkg prefix ghost_sphera_examples

if the output is *Package not found* - go back **installation** page and make sure you've completed *Installing Ghost Sphera Examples ROS2 Package*.

Run example
------------
open a new terminal and run the following command (after setup stage)

  .. code-block:: console

      ros2 launch ghost_sphera_examples twist_command_example.launch.py

* the launch file contains: spawning of 3 Vision60 robots - G1, G2, G3, and the ROS2 node that sends the twist commands to all 3.

.. raw:: html

    <div style="position: relative; height: 0; overflow: hidden; max-width: 100%; height: auto; margin-bottom: 3em;">
    <video width="100%" height="auto" controls>
    <source src="_static/videos/twist_command_demo.webm" type="video/webm">
    Your browser does not support the video tag.
    </video>    
    </div>

Code Overview
--------------

Full code
````````````

.. code-block:: cpp

    #include "rclcpp/rclcpp.hpp"
    #include "geometry_msgs/msg/twist.hpp"
    #include <math.h>

    class TwistCommandExample : public rclcpp::Node
    {
    public:
    TwistCommandExample()
        : Node("twist_command_example")
    {
        // Get topics from parameters
        std::string topic1, topic2, topic3;
        this->declare_parameter<std::string>("cmd_topic1", "/G1/mcu/command/manual_twist");
        this->declare_parameter<std::string>("cmd_topic2", "/G2/mcu/command/manual_twist");
        this->declare_parameter<std::string>("cmd_topic3", "/G3/mcu/command/manual_twist");
        this->get_parameter("cmd_topic1", topic1);
        this->get_parameter("cmd_topic2", topic2);
        this->get_parameter("cmd_topic3", topic3);

        publisher1_ = create_publisher<geometry_msgs::msg::Twist>(topic1, 1);
        publisher2_ = create_publisher<geometry_msgs::msg::Twist>(topic2, 1);
        publisher3_ = create_publisher<geometry_msgs::msg::Twist>(topic3, 1);

        timer_ = create_wall_timer(std::chrono::milliseconds(100), std::bind(&TwistCommandExample::publishTwists, this));
    }

    private:
    void publishTwists()
    {
        auto twist_msg1 = std::make_shared<geometry_msgs::msg::Twist>();
        auto twist_msg2 = std::make_shared<geometry_msgs::msg::Twist>();
        auto twist_msg3 = std::make_shared<geometry_msgs::msg::Twist>();

        // Robot 1: Move in a circle
        twist_msg1->linear.x = 1.0;
        twist_msg1->angular.z = 0.5;

        // Robot 2: Move in a square
        if (square_counter < 25)
        {
        twist_msg2->linear.x = 1.0;
        }
        else
        {
        twist_msg2->angular.z = M_PI_2 / 2.5;
        }
        square_counter = (square_counter + 1) % 50;

        // Robot 3: Move back and forth in a line
        if (move_forward_counter < 50)
        {
        twist_msg3->linear.x = 1.5;
        }
        else
        {
        twist_msg3->linear.x = -1.5;
        }
        move_forward_counter = (move_forward_counter + 1) % 100;

        // Publish Twist messages
        publisher1_->publish(*twist_msg1);
        publisher2_->publish(*twist_msg2);
        publisher3_->publish(*twist_msg3);
    }

    rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher1_;
    rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher2_;
    rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher3_;
    rclcpp::TimerBase::SharedPtr timer_;

    int square_counter = 0;
    int move_forward_counter = 0;
    };

    int main(int argc, char **argv)
    {
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<TwistCommandExample>());
    rclcpp::shutdown();
    return 0;
    }

Examine the code
``````````````````

| let's break down the twist commands code:
| at the very top we include *rclcpp/rclcpp.hpp* include which allows you to use the most common pieces of the ROS 2 system.
| next is *geometry_msgs/msg/twist.hpp*, which includes the built-in message type you will use to publish data.
| Last is math.h which we use here for the M_PI_2 macro.

.. code-block:: cpp
    
    #include "rclcpp/rclcpp.hpp"
    #include "geometry_msgs/msg/twist.hpp"
    #include <math.h>

| The Node's constructor contains the initializations of parameters,
| twist publisher for each robot, and a timer for publishing commands periodically.

.. code-block:: cpp

    public:
    TwistCommandExample()
        : Node("twist_command_example")
    {
        // Get topics from parameters
        std::string topic1, topic2, topic3;
        this->declare_parameter<std::string>("cmd_topic1", "/G1/mcu/command/manual_twist");
        this->declare_parameter<std::string>("cmd_topic2", "/G2/mcu/command/manual_twist");
        this->declare_parameter<std::string>("cmd_topic3", "/G3/mcu/command/manual_twist");
        this->get_parameter("cmd_topic1", topic1);
        this->get_parameter("cmd_topic2", topic2);
        this->get_parameter("cmd_topic3", topic3);

        publisher1_ = create_publisher<geometry_msgs::msg::Twist>(topic1, 1);
        publisher2_ = create_publisher<geometry_msgs::msg::Twist>(topic2, 1);
        publisher3_ = create_publisher<geometry_msgs::msg::Twist>(topic3, 1);

        timer_ = create_wall_timer(std::chrono::milliseconds(100), std::bind(&TwistCommandExample::publishTwists, this));
    }

| The *publishTwists* callback contains the main functionality of calculating and publishing the commands.
| Each robot has different walking pattern program: Move in a circle, Move in a square, Move back and forth in a line.

.. code-block:: cpp

    private:
    void publishTwists()
    {
        auto twist_msg1 = std::make_shared<geometry_msgs::msg::Twist>();
        auto twist_msg2 = std::make_shared<geometry_msgs::msg::Twist>();
        auto twist_msg3 = std::make_shared<geometry_msgs::msg::Twist>();

        // Robot 1: Move in a circle
        twist_msg1->linear.x = 1.0;
        twist_msg1->angular.z = 0.5;

        // Robot 2: Move in a square
        if (square_counter < 25)
        {
        twist_msg2->linear.x = 1.0;
        }
        else
        {
        twist_msg2->angular.z = M_PI_2 / 2.5;
        }
        square_counter = (square_counter + 1) % 50;

        // Robot 3: Move back and forth in a line
        if (move_forward_counter < 50)
        {
        twist_msg3->linear.x = 1.5;
        }
        else
        {
        twist_msg3->linear.x = -1.5;
        }
        move_forward_counter = (move_forward_counter + 1) % 100;

        // Publish Twist messages
        publisher1_->publish(*twist_msg1);
        publisher2_->publish(*twist_msg2);
        publisher3_->publish(*twist_msg3);
    }

| next we have declarations of class members: twist publishers, counters for calculations, and the timer.

.. code-block:: cpp

    rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher1_;
    rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher2_;
    rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher3_;
    rclcpp::TimerBase::SharedPtr timer_;

    int square_counter = 0;
    int move_forward_counter = 0;

| Finally we get to *main*, where the node actually executes.
| *rclcpp::init* initializes ROS 2, and *rclcpp::spin* starts processing data from the node, including callbacks from the timer.
.. code-block:: cpp
    
    int main(int argc, char **argv)
    {
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<TwistCommandExample>());
    rclcpp::shutdown();
    return 0;
    }