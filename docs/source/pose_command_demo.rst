 
=========================
Robot Pose commands Demo
=========================

.. contents:: Table of Contents
    :local:


Setup
-------

#. Make sure you have installed **ghost_sphera_examples** and that its ROS2 WS is sourced.
#. verify by running:

  .. code-block:: console

      ros2 pkg prefix ghost_sphera_examples

if the output is *Package not found* - go back **installation** page and make sure you've completed *Installing Ghost Sphera Examples ROS2 Package*.

Run example
------------
open a new terminal and run the following command (after setup stage)

  .. code-block:: console

      ros2 launch ghost_sphera_examples pose_command_example.launch.py

* the launch file contains: running Sphera simulation, spawning of 4 Vision60 robots - G1, G2, G3, G4, and the ROS2 node that sends the pose commands to all 4.

.. raw:: html

    <div style="position: relative; height: 0; overflow: hidden; max-width: 100%; height: auto; margin-bottom: 3em;">
    <video width="100%" height="auto" controls>
    <source src="_static/videos/pose_command_demo.webm" type="video/webm">
    Your browser does not support the video tag.
    </video>    
    </div>

Code Overview
--------------

Full code
````````````
.. code-block:: cpp

  #include "rclcpp/rclcpp.hpp"
  #include "geometry_msgs/msg/pose.hpp"
  #include "geometry_msgs/msg/quaternion.hpp"
  #include "std_msgs/msg/u_int32.hpp"

  class PoseCommandExample : public rclcpp::Node
  {
  public:
    PoseCommandExample()
        : Node("pose_command_example")
    {
      // Get topics from parameters
      std::string topic1, topic2, topic3, topic4;
      this->declare_parameter<std::string>("cmd_topic1", "/G1/mcu/command/pose");
      this->declare_parameter<std::string>("cmd_topic2", "/G2/mcu/command/pose");
      this->declare_parameter<std::string>("cmd_topic3", "/G3/mcu/command/pose");
      this->declare_parameter<std::string>("cmd_topic4", "/G4/mcu/command/pose");
      this->get_parameter("cmd_topic1", topic1);
      this->get_parameter("cmd_topic2", topic2);
      this->get_parameter("cmd_topic3", topic3);
      this->get_parameter("cmd_topic4", topic4);

      publisher1_ = create_publisher<geometry_msgs::msg::Pose>(topic1, 1);
      publisher2_ = create_publisher<geometry_msgs::msg::Pose>(topic2, 1);
      publisher3_ = create_publisher<geometry_msgs::msg::Pose>(topic3, 1);
      publisher4_ = create_publisher<geometry_msgs::msg::Pose>(topic4, 1);

      std::string set_action_topic1, set_action_topic2, set_action_topic3, set_action_topic4;
      this->declare_parameter<std::string>("set_action_topic1", "/G1/command/setAction");
      this->declare_parameter<std::string>("set_action_topic2", "/G2/command/setAction");
      this->declare_parameter<std::string>("set_action_topic3", "/G3/command/setAction");
      this->declare_parameter<std::string>("set_action_topic4", "/G4/command/setAction");
      this->get_parameter("set_action_topic1", set_action_topic1);
      this->get_parameter("set_action_topic2", set_action_topic2);
      this->get_parameter("set_action_topic3", set_action_topic3);
      this->get_parameter("set_action_topic4", set_action_topic4);

      set_action_publisher1_ = create_publisher<std_msgs::msg::UInt32>(set_action_topic1, 1);
      set_action_publisher2_ = create_publisher<std_msgs::msg::UInt32>(set_action_topic2, 1);
      set_action_publisher3_ = create_publisher<std_msgs::msg::UInt32>(set_action_topic3, 1);
      set_action_publisher4_ = create_publisher<std_msgs::msg::UInt32>(set_action_topic4, 1);

      timer_ = create_wall_timer(std::chrono::milliseconds(20), std::bind(&PoseCommandExample::publishPoses, this));
      timer_->cancel();

      set_stand_timer_ = create_wall_timer(std::chrono::milliseconds(200), std::bind(&PoseCommandExample::setStand, this));
    }

  private:
    void setStand()
    {
      auto setaction_msg = std::make_shared<std_msgs::msg::UInt32>();

      setaction_msg->data = 1;

      // command stand for all robots
      set_action_publisher1_->publish(*setaction_msg);
      set_action_publisher2_->publish(*setaction_msg);
      set_action_publisher3_->publish(*setaction_msg);
      set_action_publisher4_->publish(*setaction_msg);

      set_stand_timer_counter++;
      if (set_stand_timer_counter >= 20)
      {
        // after changing to stand -> start sending pose commands
        set_stand_timer_->cancel();
        timer_->reset();
      }
    }

    void publishPoses()
    {
      auto pose_msg1 = std::make_shared<geometry_msgs::msg::Pose>();
      auto pose_msg2 = std::make_shared<geometry_msgs::msg::Pose>();
      auto pose_msg3 = std::make_shared<geometry_msgs::msg::Pose>();
      auto pose_msg4 = std::make_shared<geometry_msgs::msg::Pose>();

      double normalized_pose = movementIncrement(timer_counter_);
      // Robot 1: Set only roll
      pose_msg1->orientation.x = normalized_pose;
      // Robot 2: Set only pitch
      pose_msg2->orientation.y = normalized_pose;
      // Robot 3: Set only yaw
      pose_msg3->orientation.z = normalized_pose;
      // Robot 4: Set only height
      pose_msg4->position.z = normalized_pose;

      // Publish Pose messages
      publisher1_->publish(*pose_msg1);
      publisher2_->publish(*pose_msg2);
      publisher3_->publish(*pose_msg3);
      publisher4_->publish(*pose_msg4);

      // Increment timer counter for movement calculation
      if (positive_direction)
      {
        timer_counter_++;
        if(timer_counter_ >= 200)
        {
          positive_direction = false;
        }
      }
      else
      {
        timer_counter_--;
        if(timer_counter_ <= -200)
        {
          positive_direction = true;
        }
      }
    }

    double movementIncrement(int time_counter)
    {
      // Increment the movement pose based on time
      // 0.02 is the timer interval in seconds
      // division by 4 to make sure pose is normalized [-1,1] 
      return time_counter * 0.02 / 4.0; 
    }


    rclcpp::Publisher<geometry_msgs::msg::Pose>::SharedPtr publisher1_;
    rclcpp::Publisher<geometry_msgs::msg::Pose>::SharedPtr publisher2_;
    rclcpp::Publisher<geometry_msgs::msg::Pose>::SharedPtr publisher3_;
    rclcpp::Publisher<geometry_msgs::msg::Pose>::SharedPtr publisher4_;

    rclcpp::Publisher<std_msgs::msg::UInt32>::SharedPtr set_action_publisher1_;
    rclcpp::Publisher<std_msgs::msg::UInt32>::SharedPtr set_action_publisher2_;
    rclcpp::Publisher<std_msgs::msg::UInt32>::SharedPtr set_action_publisher3_;
    rclcpp::Publisher<std_msgs::msg::UInt32>::SharedPtr set_action_publisher4_;

    rclcpp::TimerBase::SharedPtr timer_;
    rclcpp::TimerBase::SharedPtr set_stand_timer_;

    int timer_counter_ = 0;
    int set_stand_timer_counter = 0;

    bool positive_direction = true;
  };

  int main(int argc, char **argv)
  {
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<PoseCommandExample>());
    rclcpp::shutdown();
    return 0;
  }

Examine the code
``````````````````
| let's break down the twist commands code:
| at the very top we include *rclcpp/rclcpp.hpp* include which allows you to use the most common pieces of the ROS 2 system.
| next is the built-in messages types you will use to publish data.

.. code-block:: cpp

    #include "rclcpp/rclcpp.hpp"
    #include "geometry_msgs/msg/pose.hpp"
    #include "geometry_msgs/msg/quaternion.hpp"
    #include "std_msgs/msg/u_int32.hpp"

| The Node's constructor contains the initializations of parameters,
| setAction publishers to change the robot mode to 'Stand', 
| pose publisher for each robot, and a timer for publishing commands periodically.

.. code-block:: cpp

    public:
      PoseCommandExample()
          : Node("pose_command_example")
      {
        // Get topics from parameters
        std::string topic1, topic2, topic3, topic4;
        this->declare_parameter<std::string>("cmd_topic1", "/G1/mcu/command/pose");
        this->declare_parameter<std::string>("cmd_topic2", "/G2/mcu/command/pose");
        this->declare_parameter<std::string>("cmd_topic3", "/G3/mcu/command/pose");
        this->declare_parameter<std::string>("cmd_topic4", "/G4/mcu/command/pose");
        this->get_parameter("cmd_topic1", topic1);
        this->get_parameter("cmd_topic2", topic2);
        this->get_parameter("cmd_topic3", topic3);
        this->get_parameter("cmd_topic4", topic4);

        publisher1_ = create_publisher<geometry_msgs::msg::Pose>(topic1, 1);
        publisher2_ = create_publisher<geometry_msgs::msg::Pose>(topic2, 1);
        publisher3_ = create_publisher<geometry_msgs::msg::Pose>(topic3, 1);
        publisher4_ = create_publisher<geometry_msgs::msg::Pose>(topic4, 1);

        std::string set_action_topic1, set_action_topic2, set_action_topic3, set_action_topic4;
        this->declare_parameter<std::string>("set_action_topic1", "/G1/command/setAction");
        this->declare_parameter<std::string>("set_action_topic2", "/G2/command/setAction");
        this->declare_parameter<std::string>("set_action_topic3", "/G3/command/setAction");
        this->declare_parameter<std::string>("set_action_topic4", "/G4/command/setAction");
        this->get_parameter("set_action_topic1", set_action_topic1);
        this->get_parameter("set_action_topic2", set_action_topic2);
        this->get_parameter("set_action_topic3", set_action_topic3);
        this->get_parameter("set_action_topic4", set_action_topic4);

        set_action_publisher1_ = create_publisher<std_msgs::msg::UInt32>(set_action_topic1, 1);
        set_action_publisher2_ = create_publisher<std_msgs::msg::UInt32>(set_action_topic2, 1);
        set_action_publisher3_ = create_publisher<std_msgs::msg::UInt32>(set_action_topic3, 1);
        set_action_publisher4_ = create_publisher<std_msgs::msg::UInt32>(set_action_topic4, 1);

        timer_ = create_wall_timer(std::chrono::milliseconds(20), std::bind(&PoseCommandExample::publishPoses, this));
        timer_->cancel();

        set_stand_timer_ = create_wall_timer(std::chrono::milliseconds(200), std::bind(&PoseCommandExample::setStand, this));
      }

| setStand is the callback we initially call to command all 4 robots into 'Stand' mode.
| after 20 iterations, this timer stops and the main timer starts.

.. code-block:: cpp

    private:
      void setStand()
      {
        auto setaction_msg = std::make_shared<std_msgs::msg::UInt32>();

        setaction_msg->data = 1;

        // command stand for all robots
        set_action_publisher1_->publish(*setaction_msg);
        set_action_publisher2_->publish(*setaction_msg);
        set_action_publisher3_->publish(*setaction_msg);
        set_action_publisher4_->publish(*setaction_msg);

        set_stand_timer_counter++;
        if (set_stand_timer_counter >= 20)
        {
          // after changing to stand -> start sending pose commands
          set_stand_timer_->cancel();
          timer_->reset();
        }
      }

| The *publishPoses* callback contains the main functionality of calculating and publishing the commands.
| Each robot has different pattern program: move only yaw, move only roll, move only pitch, move only set height.

.. code-block:: cpp

    void publishPoses()
    {
      auto pose_msg1 = std::make_shared<geometry_msgs::msg::Pose>();
      auto pose_msg2 = std::make_shared<geometry_msgs::msg::Pose>();
      auto pose_msg3 = std::make_shared<geometry_msgs::msg::Pose>();
      auto pose_msg4 = std::make_shared<geometry_msgs::msg::Pose>();

      double normalized_pose = movementIncrement(timer_counter_);
      // Robot 1: Set only roll
      pose_msg1->orientation.x = normalized_pose;
      // Robot 2: Set only pitch
      pose_msg2->orientation.y = normalized_pose;
      // Robot 3: Set only yaw
      pose_msg3->orientation.z = normalized_pose;
      // Robot 4: Set only height
      pose_msg4->position.z = normalized_pose;

      // Publish Pose messages
      publisher1_->publish(*pose_msg1);
      publisher2_->publish(*pose_msg2);
      publisher3_->publish(*pose_msg3);
      publisher4_->publish(*pose_msg4);

      // Increment timer counter for movement calculation
      if (positive_direction)
      {
        timer_counter_++;
        if(timer_counter_ >= 200)
        {
          positive_direction = false;
        }
      }
      else
      {
        timer_counter_--;
        if(timer_counter_ <= -200)
        {
          positive_direction = true;
        }
      }
    }

| Helper functions to calculate pose change based on current time
.. code-block:: cpp

    double movementIncrement(int time_counter)
    {
      // Increment the movement pose based on time
      // 0.02 is the timer interval in seconds
      // division by 4 to make sure pose is normalized [-1,1] 
      return time_counter * 0.02 / 4.0; 
    }

| Declarations of class members: pose publishers, setAction publishers, counters for calculations, and the timers.

.. code-block:: cpp

      rclcpp::Publisher<geometry_msgs::msg::Pose>::SharedPtr publisher1_;
      rclcpp::Publisher<geometry_msgs::msg::Pose>::SharedPtr publisher2_;
      rclcpp::Publisher<geometry_msgs::msg::Pose>::SharedPtr publisher3_;
      rclcpp::Publisher<geometry_msgs::msg::Pose>::SharedPtr publisher4_;

      rclcpp::Publisher<std_msgs::msg::UInt32>::SharedPtr set_action_publisher1_;
      rclcpp::Publisher<std_msgs::msg::UInt32>::SharedPtr set_action_publisher2_;
      rclcpp::Publisher<std_msgs::msg::UInt32>::SharedPtr set_action_publisher3_;
      rclcpp::Publisher<std_msgs::msg::UInt32>::SharedPtr set_action_publisher4_;

      rclcpp::TimerBase::SharedPtr timer_;
      rclcpp::TimerBase::SharedPtr set_stand_timer_;

      int timer_counter_ = 0;
      int set_stand_timer_counter = 0;

      bool positive_direction = true;

| Finally we get to *main*, where the node actually executes.
| *rclcpp::init* initializes ROS 2, and *rclcpp::spin* starts processing data from the node, including callbacks from the timer.
.. code-block:: cpp

    int main(int argc, char **argv)
    {
      rclcpp::init(argc, argv);
      rclcpp::spin(std::make_shared<PoseCommandExample>());
      rclcpp::shutdown();
      return 0;
    }