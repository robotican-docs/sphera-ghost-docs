 
================
Spawning Objects
================

.. contents:: Table of Contents
    :local:

How to use the spawn objects menu
-------------------------------------

#. First open Sphera simulation and choose desired level.
#. now press `K`, choose objects from the opened menu, and place it in the environment by using right-click.

.. image:: images/spawn_objects.png
   :align: center
   :width: 500

* `range`: the max range your curser should place an object in.
* `visualize`: wheather or not to show you where you are placing an object (also visable to camera sensors)
* `physics`: checked is for movable objects' un-checked is for static objects.
* You can also see the coordinates you are pointing at while using this tool.

.. raw:: html

    <div style="position: relative; height: 0; overflow: hidden; max-width: 100%; height: auto; margin-bottom: 3em;">
    <video width="100%" height="auto" controls>
    <source src="_static/videos/spawn_objects_demo.webm" type="video/webm">
    Your browser does not support the video tag.
    </video>    
    </div>

How to spawn objects using ROS2
-------------------------------------

Setup
````````

#. Make sure you have installed **ghost_sphera_examples** and that its ROS2 WS is sourced.
#. verify by running:

  .. code-block:: console

      ros2 pkg prefix ghost_sphera_examples

if the output is *Package not found* - go back **installation** page and make sure you've completed *Installing Ghost Sphera Examples ROS2 Package*.

Run example
`````````````

#. spawn an object by running:
  .. code-block:: console

      ros2 launch ghost_sphera_examples spawn_example.launch.py

#. control parameters by running:
  .. code-block:: console

      ros2 run ghost_sphera_examples static_object_spawner_node.py --ros-args -p name:="MyOilDrum" -p skin:="OilDrum" -p location.x:=3.0 -p location.y:=2.0 -p location.z:=1.0 -p enable_physics:=False -p user:="$USER"

.. list-table:: Object Spawn Options
   :widths: 1 1 1 1
   :header-rows: 1

   * - option
     - value
     - default
     - description
   * - user 
     - string
     - "" (empty string means the taking the computer's user name)
     - the name of sphera simulation user to spawn in
   * - name 
     - string
     - "StaticObject1"
     - the name of object to be spawned. if object with this name already exists - replace it.
   * - skin 
     - string - must be one of ['WoodenCrate', 'MilitaryCrate', 'OilDrum', 'Rock01', 'Weapon']
     - "WoodenCrate"
     - the object type
   * - enable_physics 
     - boolean
     - True
     - if the object is movable and can interact vith the environment
   * - location.x 
     - float
     - 2.0
     - the object's X location with respect to the level's origin.
   * - location.y 
     - float
     - 2.0
     - the object's Y location with respect to the level's origin.
   * - location.z 
     - float
     - 4.0
     - the object's Z location with respect to the level's origin.