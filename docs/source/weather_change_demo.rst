 
====================
Weather Change Demo
====================

.. contents:: Table of Contents
    :local:



Setup
-------

#. Make sure you have installed **ghost_sphera_examples** and that its ROS2 WS is sourced.
#. verify by running:

  .. code-block:: console

      ros2 pkg prefix ghost_sphera_examples

if the output is *Package not found* - go back **installation** page and make sure you've completed *Installing Ghost Sphera Examples ROS2 Package*.

Run example 
------------
1. open a new terminal and run the following command:

  .. code-block:: console

      ros2 launch ghost_sphera_examples weather_example.launch.py

Code Overview
--------------

Full launch file
``````````````````
.. code-block:: python

    #!/usr/bin/env python3

    import getpass
    import random    
    from launch import LaunchDescription
    from launch.actions import  ExecuteProcess, DeclareLaunchArgument                            
    from launch.conditions import IfCondition
    from launch.substitutions import LaunchConfiguration  

    WeatherTypes = [
        "light rain",
        "rain",
        "thunderstorm",
        "light snow",
        "snow",
        "blizzard",
        "dust storm",
        "dust",
        "foggy",
        "clear skies",
        "partly cloudy",
        "cloudy",
        "overcast"
        ]

        
    def get_random_time_of_day():
        hours = random.randint(6, 18) # leaving out the darkest hours
        minutes = random.randint(0, 59)
        seconds = random.randint(0, 59)
        return f"{hours:02}:{minutes:02}:{seconds:02}"

    def generate_random_weather(sleep_time: int):
        # Choose a random weather type 
        weather = random.choice(list(WeatherTypes))

        # Generate a random time_of_day
        time_of_day = get_random_time_of_day()

        # Generate random wind speed and direction
        wind_speed = random.randint(0, 15)
        wind_direction = random.randint(0, 360)

        # return a terminal command to be executed with the rught delay and parameters
        return f'sleep {sleep_time} && ros2 run ghost_sphera_examples sphera_set_node.py --ros-args -p weather:="{weather}" -p time_of_day:="{time_of_day}" -p wind_direction:={wind_direction} -p wind_speed:={wind_speed}'

    def generate_launch_description():
        run_sphera = DeclareLaunchArgument('run_sphera', default_value='true')
        sphera = ExecuteProcess(
            cmd=[[f'sphera -user={getpass.getuser()} -map=Flat -origin=31.363789,34.877132,442.5']],
            shell=True,
            condition=IfCondition(LaunchConfiguration('run_sphera'))
        )

        # time to wait between changes of weather
        change_dt = 5
        # number of generated weather changes
        num_of_changes = 10
        # the generated list to be executed
        random_weather_array =[ExecuteProcess(
            cmd=[generate_random_weather(i*change_dt + change_dt)], shell=True) for i in range(num_of_changes)]

        return LaunchDescription([run_sphera, sphera] + random_weather_array)



Examine the code
``````````````````
| let's in break down:
| first we import all needed packages:

.. code-block:: python

    #!/usr/bin/env python3

    import getpass
    import random    
    from launch import LaunchDescription
    from launch.actions import  ExecuteProcess, DeclareLaunchArgument                            
    from launch.conditions import IfCondition
    from launch.substitutions import LaunchConfiguration  

| then we define all available weather types:

.. code-block:: python

    WeatherTypes = [
        "light rain",
        "rain",
        "thunderstorm",
        "light snow",
        "snow",
        "blizzard",
        "dust storm",
        "dust",
        "foggy",
        "clear skies",
        "partly cloudy",
        "cloudy",
        "overcast"
        ]

| next we have a helper functions to generate calls for random weather conditions.

.. code-block:: python

    def get_random_time_of_day():
        hours = random.randint(6, 18) # leaving out the darkest hours
        minutes = random.randint(0, 59)
        seconds = random.randint(0, 59)
        return f"{hours:02}:{minutes:02}:{seconds:02}"

    def generate_random_weather(sleep_time: int):
        # Choose a random weather type 
        weather = random.choice(list(WeatherTypes))

        # Generate a random time_of_day
        time_of_day = get_random_time_of_day()

        # Generate random wind speed and direction
        wind_speed = random.randint(0, 15)
        wind_direction = random.randint(0, 360)

        # return a terminal command to be executed with the rught delay and parameters
        return f'sleep {sleep_time} && ros2 run ghost_sphera_examples sphera_set_node.py --ros-args -p weather:="{weather}" -p time_of_day:="{time_of_day}" -p wind_direction:={wind_direction} -p wind_speed:={wind_speed}'

| here we run the Sphera simulation with given parameters:

.. code-block:: python
    
    def generate_launch_description():
        run_sphera = DeclareLaunchArgument('run_sphera', default_value='true')
        sphera = ExecuteProcess(
            cmd=[[f'sphera -user={getpass.getuser()} -map=Flat -origin=31.363789,34.877132,442.5']],
            shell=True,
            condition=IfCondition(LaunchConfiguration('run_sphera'))
        )

| then we generate a list of calls to run the *SpheraSetCaller* which calls the */sphera/<user_name>/set*, every time with randomly generated parameters.
| here we chose to have 10 changes, with 5 seconds delay between them

.. code-block:: python

    # time to wait between changes of weather
    change_dt = 5
    # number of generated weather changes
    num_of_changes = 10
    # the generated list to be executed
    random_weather_array =[ExecuteProcess(
        cmd=[generate_random_weather(i*change_dt + change_dt)], shell=True) for i in range(num_of_changes)]

| finally we return the list to be executed in sequence when running this launch file

.. code-block:: python

    return LaunchDescription([run_sphera, sphera] + random_weather_array)