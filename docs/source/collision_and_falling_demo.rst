 
==============================
Collision & falling detection 
==============================

.. contents:: Table of Contents
    :local:



Setup
-------

#. Make sure you have installed **ghost_sphera_examples** and that its ROS2 WS is sourced.
#. verify by running:

  .. code-block:: console

      ros2 pkg prefix ghost_sphera_examples

if the output is *Package not found* - go back **installation** page and make sure you've completed *Installing Ghost Sphera Examples ROS2 Package*.

Run example
------------
open a new terminal and run the following command:

  .. code-block:: console

      ros2 launch ghost_sphera_examples collision_detection_example.launch.py

.. raw:: html

    <div style="position: relative; height: 0; overflow: hidden; max-width: 100%; height: auto; margin-bottom: 3em;">
    <video width="100%" height="auto" controls>
    <source src="_static/videos/collision_and_falling_detection.webm" type="video/webm">
    Your browser does not support the video tag.
    </video>    
    </div>

Code Overview
--------------

Full node file
``````````````````
.. code-block:: python

    #!/usr/bin/env python3

    import rclpy
    from rclpy.node import Node
    from geometry_msgs.msg import Point
    from std_msgs.msg import String
    from visualization_msgs.msg import Marker
    from sphera_common_interfaces.msg import SpheraPawnState
    import json

    class CollisionMarkerPublisher(Node):
        def __init__(self):
            super().__init__('collision_marker_publisher')

            self.declare_parameters(
                namespace='',
                parameters=[
                    ('collision_topic', "/G1/collision"),
                    ('marker_topic', "/G1/collision_marker"), 
                    ('pawn_state_topic', "/G1/sphera/state"), 
                    ('pawn_state_marker_topic', "/G1/state_marker"), 
                    ]
            )
            self.collision_sub = self.create_subscription(
                String,
                self.get_parameter("collision_topic").get_parameter_value().string_value,
                self.collision_callback,
                1)
            self.pawn_state_sub = self.create_subscription(
                SpheraPawnState,
                self.get_parameter("pawn_state_topic").get_parameter_value().string_value,
                self.pawn_state_callback,
                10)

            # Create a publisher for the marker arrow
            self.collision_marker_pub = self.create_publisher(Marker, self.get_parameter("marker_topic").get_parameter_value().string_value, 10)
            # Create a publisher for the marker text
            self.state_marker_pub = self.create_publisher(Marker, self.get_parameter("pawn_state_marker_topic").get_parameter_value().string_value, 10)

        def collision_callback(self, msg):
            try:
                # Parse the JSON string
                data = json.loads(msg.data)

                # Extract ImpactPoint and Normal from the JSON data
                impact_point_str = data.get('ImpactPoint', 'X=0 Y=0 Z=0')
                normal_str = data.get('Normal', 'X=1 Y=0 Z=0')

                # Extract coordinates from ImpactPoint and Normal strings
                impact_point_coords = [float(coord.split('=')[1]) for coord in impact_point_str.split()]
                normal_coords = [float(coord.split('=')[1]) for coord in normal_str.split()]

                # Create a Marker message for visualization
                marker = Marker()
                marker.header.frame_id = 'map'
                marker.header.stamp = self.get_clock().now().to_msg()
                marker.type = Marker.ARROW
                marker.action = Marker.ADD

                # Set arrow start point based on the ImpactPoint
                impact_point = Point()
                impact_point.x = impact_point_coords[0]
                impact_point.y = impact_point_coords[1]
                impact_point.z = impact_point_coords[2]
                marker.points.append(impact_point)

                # Set arrow end point based on the Normal direction
                arrow_length = 0.5
                end_point = Point()
                end_point.x =  impact_point_coords[0] + arrow_length * normal_coords[0]
                end_point.y = impact_point_coords[1] + arrow_length * normal_coords[1]

                # Z direction is zero for visualization. uncomment next line to add it back
                end_point.z = impact_point.z
                # end_point.z = impact_point_coords[2] + arrow_length * normal_coords[2]
                marker.points.append(end_point)

                # Set arrow properties
                marker.scale.x = 0.06 
                marker.scale.y = 0.12 
                marker.scale.z = 0.3 
                marker.color.r = 1.0
                marker.color.g = 0.0
                marker.color.b = 0.0
                marker.color.a = 1.0
                marker.lifetime.sec = 3

                # Publish the marker
                self.collision_marker_pub.publish(marker)

            except json.JSONDecodeError as e:
                self.get_logger().error(f"Failed to parse JSON string: {str(e)}")

        def pawn_state_callback(self, msg):
            # Create text marker
            text_marker = Marker()
            text_marker.header.frame_id = 'base_link' 
            text_marker.header.stamp = self.get_clock().now().to_msg()
            text_marker.type = Marker.TEXT_VIEW_FACING
            text_marker.action = Marker.ADD

            # get text from state
            text_marker.text = msg.strings[0] # Sit / Stand / Walk / Falling

            # Set text properties
            text_marker.pose.orientation.w = 1.0
            text_marker.scale.x = 0.3
            text_marker.scale.y = 0.3
            text_marker.scale.z = 0.3

            # text in red if robot is falling
            text_marker.color.r = 1.0
            if text_marker.text != 'Falling':
                text_marker.color.g = 1.0
                text_marker.color.b = 1.0
            text_marker.color.a = 1.0
            
            text_marker.lifetime.sec = 1 

            # Publish the marker
            self.state_marker_pub.publish(text_marker)


    def main(args=None):
        rclpy.init(args=args)
        collision_marker_publisher = CollisionMarkerPublisher()
        rclpy.spin(collision_marker_publisher)

        collision_marker_publisher.destroy_node()
        rclpy.shutdown()

    if __name__ == '__main__':
        main()


Examine the code
````````````````````
| let's in break down:
| first we import all needed packages:

.. code-block:: python

    #!/usr/bin/env python3

    import rclpy
    from rclpy.node import Node
    from geometry_msgs.msg import Point
    from std_msgs.msg import String
    from visualization_msgs.msg import Marker
    from sphera_common_interfaces.msg import SpheraPawnState
    import json

next we declare parameters, get the input & output topics, and initialize subscribers & publishers

.. code-block:: python

    class CollisionMarkerPublisher(Node):
        def __init__(self):
            super().__init__('collision_marker_publisher')

            self.declare_parameters(
                namespace='',
                parameters=[
                    ('collision_topic', "/G1/collision"),
                    ('marker_topic', "/G1/collision_marker"), 
                    ('pawn_state_topic', "/G1/sphera/state"), 
                    ('pawn_state_marker_topic', "/G1/state_marker"), 
                    ]
            )
            self.collision_sub = self.create_subscription(
                String,
                self.get_parameter("collision_topic").get_parameter_value().string_value,
                self.collision_callback,
                1)
            self.pawn_state_sub = self.create_subscription(
                SpheraPawnState,
                self.get_parameter("pawn_state_topic").get_parameter_value().string_value,
                self.pawn_state_callback,
                10)

            # Create a publisher for the marker arrow
            self.collision_marker_pub = self.create_publisher(Marker, self.get_parameter("marker_topic").get_parameter_value().string_value, 10)
            # Create a publisher for the marker text
            self.state_marker_pub = self.create_publisher(Marker, self.get_parameter("pawn_state_marker_topic").get_parameter_value().string_value, 10)


| the **collision_callback** gets a string containing JSON format of the collision info, from there we take the collision point - 'ImpactPoint', and the normal - 'Normal' (in map frame)
| then create an arrow marker to visualize the each collision
| in this example we discard the normal Z to make visualization nicer, but it can be visualized by uncommenting *end_point.z = impact_point_coords[2] + arrow_length * normal_coords[2]*

.. code-block:: python

        def collision_callback(self, msg):
            try:
                # Parse the JSON string
                data = json.loads(msg.data)

                # Extract ImpactPoint and Normal from the JSON data
                impact_point_str = data.get('ImpactPoint', 'X=0 Y=0 Z=0')
                normal_str = data.get('Normal', 'X=1 Y=0 Z=0')

                # Extract coordinates from ImpactPoint and Normal strings
                impact_point_coords = [float(coord.split('=')[1]) for coord in impact_point_str.split()]
                normal_coords = [float(coord.split('=')[1]) for coord in normal_str.split()]

                # Create a Marker message for visualization
                marker = Marker()
                marker.header.frame_id = 'map'
                marker.header.stamp = self.get_clock().now().to_msg()
                marker.type = Marker.ARROW
                marker.action = Marker.ADD

                # Set arrow start point based on the ImpactPoint
                impact_point = Point()
                impact_point.x = impact_point_coords[0]
                impact_point.y = impact_point_coords[1]
                impact_point.z = impact_point_coords[2]
                marker.points.append(impact_point)

                # Set arrow end point based on the Normal direction
                arrow_length = 0.5
                end_point = Point()
                end_point.x =  impact_point_coords[0] + arrow_length * normal_coords[0]
                end_point.y = impact_point_coords[1] + arrow_length * normal_coords[1]

                # Z direction is zero for visualization. uncomment next line to add it back
                end_point.z = impact_point.z
                # end_point.z = impact_point_coords[2] + arrow_length * normal_coords[2]
                marker.points.append(end_point)

                # Set arrow properties
                marker.scale.x = 0.06 
                marker.scale.y = 0.12 
                marker.scale.z = 0.3 
                marker.color.r = 1.0
                marker.color.g = 0.0
                marker.color.b = 0.0
                marker.color.a = 1.0
                marker.lifetime.sec = 3

                # Publish the marker
                self.collision_marker_pub.publish(marker)

| the **pawn_state_callback** gets ground truth about the robot state, which include a string indicating if the robot is in:  **Sit / Stand / Walk / Falling**
| then create a text marker to visualize the given text and make the text red if falling

.. code-block:: python

        def pawn_state_callback(self, msg):
            # Create text marker
            text_marker = Marker()
            text_marker.header.frame_id = 'base_link' 
            text_marker.header.stamp = self.get_clock().now().to_msg()
            text_marker.type = Marker.TEXT_VIEW_FACING
            text_marker.action = Marker.ADD

            # get text from state
            text_marker.text = msg.strings[0] # Sit / Stand / Walk / Falling

            # Set text properties
            text_marker.pose.orientation.w = 1.0
            text_marker.scale.x = 0.3
            text_marker.scale.y = 0.3
            text_marker.scale.z = 0.3

            # text in red if robot is falling
            text_marker.color.r = 1.0
            if text_marker.text != 'Falling':
                text_marker.color.g = 1.0
                text_marker.color.b = 1.0
            text_marker.color.a = 1.0
            
            text_marker.lifetime.sec = 1 

            # Publish the marker
            self.state_marker_pub.publish(text_marker)


Collision string format
------------------------

| The collision string message is in JSON format of the following structure:
| ``{"OtherActor":"Cube","Normal":"X=0.814 Y=0.193 Z=0.548","ImpactPoint":"X=12.900 Y=2.500 Z=0.987"}``

.. list-table:: 
   :widths: 1 1 1
   :header-rows: 1

   * - key
     - value
     - meaning
   * - OtherActor 
     - string
     - the name of the entity the robot colide with
   * - Normal (X, Y, Z)
     - 3 floats
     - Normalized normal vector of the collision (in map frame)
   * - ImpactPoint (X, Y, Z)
     - 3 floats
     - Location of the collision (in map frame)