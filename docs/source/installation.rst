 
================
Installation
================

.. contents:: Table of Contents
    :local:
    
System Requirements
----------------------

* We support Ubuntu 22.04 OS on 64-bit x86.
* Nvidia graphics card with installed drivers is required. (minimum is GeForce RTX 30 Series, the 40 Series is recommended)
* Minimum 16GB CPU RAM (32GB is recommended)
* Free disk storage > 8GB



Installing ROS2
------------------

Please follow the official `ROS2 Humble installation instructions  <https://docs.ros.org/en/humble/Installation/Ubuntu-Install-Debians.html>`_.


Installing Sphera
-------------------------

#. Download the distributed files to a directory on your computer.

   | from the list of available files you need to download the following:
   | ``install.sh`` , ``install_geographiclib_dataset.sh``, ``ghost_sphera_examples.tar.gz``,
   | ROS2 packages for your chosen distro ``ros-humble-*``,
   | Sphera simulation ``sphera_1.*_Ghost_jammy_amd64.deb``,

   .. image:: images/drive_files.png
    :align: center
    :width: 600

#. Open a terminal in the directory you chose.
#. Source your ROS2 setup.bash

    .. code-block:: console

        source /opt/ros/humble/setup.bash

#. Run installation script - this installs required libraries, packages and the Sphera software.

    .. code-block:: console

        ./install.sh

#. Setup licence by running sphera for the first time in a new terminal:

    .. code-block:: console

        sphera

then choose **Options** and set the path to the **licence.bin** file that has been distributed to you. 


Installing Ghost Sphera Examples ROS2 Package
-------------------------------------------------

#. Install additional ROS2 packages:

    .. code-block:: console

        sudo apt install ros-humble-image-view ros-humble-gscam ros-humble-rviz2

#. Create new ROS2 workspace (or use an existing one)

    .. code-block:: console

        mkdir -p ~/sphera_ws/src

#. extract sphera_ghost_examples package into your ROS2 workspace src folder (e.g ~/sphera_ws/src)

    .. code-block:: console

        tar -xf ghost_sphera_examples.tar.gz -C ~/sphera_ws/src

#. Build workspace (make sure your ROS2 setup.bash is sourced in the current terminal before this step)

    .. code-block:: console

            cd ~/sphera_ws
            colcon build --symlink-install

#. Source the new workspace
    
    .. code-block:: console

        source ~/sphera_ws/install/setup.bash

    Consider adding the above command to your ~/.bashrc to avoid running it manually on each new terminal.