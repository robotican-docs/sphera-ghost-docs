
Welcome to Sphera Ghost's documentation!
===========================================

.. image:: images/logo_sphera.png
   :align: center
   :width: 1400


.. toctree::
   :maxdepth: 1
   :caption: Contents:

   installation
   getting_started
   spawning_robots
   spawning_objects
   manual_robot_control
   ros2_interfaces
   twist_command_demo
   pose_command_demo
   rgb_cam_demo
   lidar_and_depth_demo
   weather_change_demo
   ai_nav
   collision_and_falling_demo
   multiplayer_demo
   troubleshooting
   changelog

